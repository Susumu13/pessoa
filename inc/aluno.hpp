#ifndef ALUNO_H
#define ALUNO_H

#include "pessoa.hpp"

class Aluno: public Pessoa
{
	private:
		int matricula;
		int quantidade_de_creditos;
		int semestre;
		float ira;
	public:
		Aluno();
		Aluno(string nome,string idade,string telefone,int matricula );
		void setMatricula(int matricula);
		int getMatricula();
		void setQuantidadeCreditos(int creditos);
		int getQuantidadeCreditos();
		void setSemestre(int semestre);
		int getSemestre();
		void setIra(float ira);
		float getIra();
};

#endif
