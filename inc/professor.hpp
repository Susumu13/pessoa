#ifndef PROFESSOR_H
#define PROFESSOR_H

#include "pessoa.hpp"

using namespace std;

class Professor: public Pessoa
{
	private:
		int matricula_func;
		int disciplinas;
		int qtde_alunos;
		int projetos;
	public:
		Professor();
		Professor(string nome,string idade, string telefone,int matricula_func,int disciplinas, int qtde_alunos,int projetos);
		void setMatricula_func(int matricula_func);
		int getMatricula_func();
		void setDisciplinas(int disciplinas);
		int getDisciplinas();
		void setQtde_alunos(int qtde_alunos);
		int getQtde_alunos();
		void setProjetos(int projetos);
		int getProjetos();
};

#endif
