#include <iostream>
#include "professor.hpp"

using namespace std;

Professor :: Professor()
{
        setNome("");
        setIdade("");
        setTelefone("");
        setMatricula_func(0);
	setDisciplinas(0);
	setQtde_alunos(0);
	setProjetos(0);
}

Professor :: Professor(string nome,string idade, string telefone,int matricula_func,int disciplinas, int qtde_alunos,int projetos)
{
        setNome(nome);
        setIdade(idade);
        setTelefone(telefone);
        setMatricula_func(matricula_func);
        setDisciplinas(disciplinas);
        setQtde_alunos(qtde_alunos);
        setProjetos(projetos);

}

void Professor::setMatricula_func(int matricula)
{
        this -> matricula_func = matricula_func;
}

int Professor::getMatricula_func()
{
        return matricula_func;
}

void Professor::setDisciplinas(int disciplinas)
{
	this -> disciplinas = disciplinas;
}

int Professor::getDisciplinas()
{
        return disciplinas;
}

void Professor::setQtde_alunos(int qtde_alunos)
{
        this -> qtde_alunos = qtde_alunos;
}

int Professor::getQtde_alunos()
{
        return qtde_alunos;
}

void Professor::setProjetos(int projetos)
{
        this -> projetos = projetos;
}

int Professor::getProjetos()
{
        return projetos;
}

