#include <iostream>
#include "aluno.hpp"

Aluno :: Aluno()
{
	setNome("");
	setIdade("");
	setTelefone("");
	setMatricula(0);
}

Aluno::Aluno(string nome, string idade, string telefone, int matricula)
{
	setNome(nome);
	setIdade(idade);
	setTelefone(telefone);
	setMatricula(matricula);
}

void Aluno::setMatricula(int matricula)
{
	this -> matricula = matricula;
}

int Aluno::getMatricula()
{
	return matricula;
}

void Aluno::setQuantidadeCreditos(int creditos)
{
	quantidade_de_creditos = creditos; //Nome diferente, nao precisa do this
}

int Aluno::getQuantidadeCreditos()
{
	return quantidade_de_creditos;
}

void Aluno::setSemestre(int semestre)
{
	this -> semestre = semestre;
}


int Aluno::getSemestre()
{
	return semestre;
}
